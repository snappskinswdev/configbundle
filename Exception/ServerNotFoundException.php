<?php
/**
 * Created by PhpStorm.
 * User: Pavel Hudinsky
 * Email: p.hudinsky@gmail.com
 * Date: 5/8/2015
 * Time: 5:22 PM
 */

namespace Snappskin\ConfigBundle\Exception;

class ServerNotFoundException extends ConfigException {

}
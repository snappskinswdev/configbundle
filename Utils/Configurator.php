<?php
/**
 * Created by PhpStorm.
 * User: Pavel Hudinsky
 * Email: p.hudinsky@gmail.com
 * Date: 5/8/2015
 * Time: 5:10 PM
 */

namespace Snappskin\ConfigBundle\Utils;

use Snappskin\ConfigBundle\Exception\ConfigException;
use Snappskin\ConfigBundle\Exception\ServerNotFoundException;

class Configurator {

    /**
     * @var array
     */
    private $config;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @param $host
     * @return string
     */
    public function buildValidateSnaTokenUrl($host)
    {
        $hostWithoutSubdomain = $this->cutSubdomainIfExists($host);
        $hostConfig = $this->getAdminServerConfig($hostWithoutSubdomain);
        $url = sprintf("%s://%s%s", $hostConfig['protocol'], $host, $hostConfig['validateSnaTokenUri']);

        return $url;
    }

    /**
     * @param $host
     * @return string
     */
    public function buildValidateAccountTokenUrl($host)
    {
        $hostWithoutSubdomain = $this->cutSubdomainIfExists($host);
        $hostConfig = $this->getAdminServerConfig($hostWithoutSubdomain);
        $url = sprintf("%s://%s%s", $hostConfig['protocol'], $host, $hostConfig['validateAccountTokenUri']);

        return $url;
    }

    private function cutSubdomainIfExists($host)
    {
        $result = preg_match('/^(?<subdomain>(\w|-)+\.)?(?<domain>(\w|-)+\.(\w|-)+)$/', $host, $data);
        if (!$result) {
            throw new ConfigException(sprintf('Invalid host format "%s".', $host));
        }
        return $data['domain'];
    }

    /**
     * @param $host
     * @return mixed
     */
    private function getAdminServerConfig($host)
    {
        $adminServers = $this->config['admin_servers'];

        if (!array_key_exists($host, $adminServers)) {
            throw new ServerNotFoundException(sprintf("Admin server with host '%s' not found in configuration.", $host));
        }

        return $adminServers[$host];
    }
}